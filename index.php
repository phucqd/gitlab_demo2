<?php

namespace App\Jobs;

use App\Exceptions\CustomException;
use App\Libraries\XmlReport;
use App\Models\Queues;
use Github\Client;
use Gitlab\Client as GitlabClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CommentGithub implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $reportQueue;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Queues $queue)
    {
        $this->reportQueue = $queue;
    }


    public function handle()
    {
        $queues = $this->reportQueue;
        $repoOwner = $queues->repo_owner;
        $repoName = $queues->repo_name;
        $buildNumber = $queues->build_number;
        $sha = $queues->commit;
        $styles = config('report.styles');
        $serviceType = auth()->user()->service_type;

        try {
            if ($serviceType == config('const.service_type.github')) {
                $client = new Client();
                $token = env('GITHUB_BOT_SOCIALITE_TOKEN');
                $client->authenticate($token, null, Client::AUTH_HTTP_TOKEN);
            } else {
                $client = new GitlabClient();
                $token = env('GITLAB_BOT_SOCIALITE_TOKEN');
                $client->authenticate($token, GitlabClient::AUTH_OAUTH_TOKEN);
            }

            $positions = $this->diffFileGithub($repoOwner, $repoName, $sha, $client);
            foreach ($styles as $style) {
                if (!in_array($style, ['phpcs', 'eslint', 'rubocop', 'android-lint', 'swift-lint'])) {
                    continue;
                }

                $fileName = $style . '.xml';
                $f = '/reports/' . $repoOwner . '/' .
                    $repoName . '/' . $buildNumber . '/reports/' . $fileName;

                if (!file_exists(storage_path('app' . $f))) {
                    continue;
                }

                $contents = Storage::get($f);
                $xmlReport = new XmlReport();

                $res = $xmlReport->{getFuncFromSlug($style)}($contents);

                foreach ($res as $report) {
                    if ((int)$report['errors'] != 0) {
                        foreach ($report['details'] as $key => $value) {
                            $body = sprintf('**Reporter**: %s%s', 'CHECKSTYLE', '</br>') .
                                sprintf('**Rule**: %s%s', $value['source'], '</br>') .
                                sprintf('**Severity**: %s%s', $value['severity'], '</br>') .
                                sprintf('**File**: %s%s', $report['name'], '</br>')
                                . (isset($value['message']) ? $value['message'] : null);

                            $fileIndex = str_replace(['/', '\/'], ['', ''], $report['name']);

                            if (!isset($positions["{$fileIndex}"]) ||
                                !isset($positions["{$fileIndex}"][$value['line']])) {
                                continue;
                            }

                            $params = [
                                'body' => $body,
                                'commit_id' => $queues->commit,
                                'path' => $report['name'],
                                'position' => (int) $positions["{$fileIndex}"][$value['line']],
                            ];

                            $client->api('pr')->comments()
                            ->create($queues->repo_owner, $queues->repo_name, $queues->pull_request_number, $params);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            logger('Comment github log: ' . $e->getMessage());
        }
    }

    public function diffFileGithub($owner, $repo, $sha, $client)
    {
        $response = $client->api('repos')->commits()->show($owner, $repo, $sha);
        $positions = [];
        $regex = '/^@@\s*(-|\+)\d*/';
        $number = '/\d{1,}/';
        $sub = '/^\-/';

        foreach ($response['files'] as $file) {
            $patch = $file['patch'];
            $codes = explode("\n", $patch);
            $fileStart = 0;
            $diffStart = -1;

            foreach ($codes as $key => $code) {
                preg_match($regex, $code, $matches);
                if (count($matches)) {
                    preg_match($number, $matches[0], $startFile);
                    $fileStart = isset($startFile[0]) ? (int) $startFile[0] - 2 : $fileStart;
                }

                $diffStart ++;
                if (preg_match($sub, $code)) {
                    continue;
                }

                $fileStart ++;

                $fileName = str_replace(['/', '\/'], ['', ''], $file['filename']);

                $positions["{$fileName}"][$fileStart] =  $diffStart;
            }
        }

        return $positions;
    }
}
